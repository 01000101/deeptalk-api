// UUID
var uuid = require('node-uuid');
// Transport security
var http = require('http');
// Logging
var bunyan = require('bunyan');
// Argument parsing
var parseArgs = require('minimist')
// Redis (for AWS ElastiCache)
var redis = require('redis')

// Configure the logging service
var log = bunyan.createLogger({
    name: 'deeptalk.api',
    src: false,
    level: 'debug',
    streams: [{
        path: 'deeptalk_api.log',
        level: 'debug'
    }]
});
var log_cache = log.child({subsystem: 'redis-cache-store'})

var server = http.createServer();

// Process service arguments
var argv = parseArgs(process.argv.slice(2));
log.debug({argv: argv}, 'Processing arguments');
var SVC_PORT = parseInt(argv['port']) || 9000;
var REDIS_URL = argv['redis_url'];
var REDIS_PORT = parseInt(argv['redis_port']) || 6379;

if (!REDIS_URL) {
    log.error('REDIS_URL is not defined');
    process.exit(-1);
}

// Cache store client
var cache = redis.createClient(
    REDIS_PORT,
    REDIS_URL,
    {no_ready_check: true});

// Create our Socket.IO instance
var io = require('socket.io').listen(server);
io.adapter(require('socket.io-redis')({
    host: REDIS_URL,
    port: REDIS_PORT
}));

var metadata = [];


function getRoomMembership(room, cb) {
    // Return a list of all members in the room (array of socket IDs)
    cache.lrange('deeptalk#/#' + room + '#members', 0, -1, function(err, data) {
        log_cache.debug({
            method: 'getRoomMembership',
            args: {room: room},
            err: err, data: data
        })
        if (err) {
            log_cache.error({err: err})
            cb([])
        } else {
            cb(data)
        }
    })
}

function addMemberToRoom(sck, room, cb) {
    // Actually join the user to the room
    sck.join(room);
    // Update the cache store, returning the current number of members
    cache.lpush('deeptalk#/#' + room + '#members', sck.id, function(err, data) {
        log_cache.debug({
            method: 'addMemberToRoom',
            args: {socket: sck.id, room: room},
            err: err, data: data
        })
        if (err) {
            log_cache.error({err: err})
            cb(-1)
        } else {
            cb(data)
        }
    })
}

function removeMemberFomRoom(sck, room, cb) {
    // Update the cache store, returning the current number of members
    cache.lrem('deeptalk#/#' + room + '#members', 0, sck.id, function(err, data) {
        log_cache.debug({
            method: 'removeMemberFomRoom',
            args: {socket: sck.id, room: room},
            err: err, data: data
        })
        if (err) {
            log_cache.error({err: err})
            cb(-1)
        } else {
            cb(data)
        }
    })
}

function hasMembershipToRoom(sck_id, room, cb) {
    getRoomMembership(room, function(members) {
        cb(members.indexOf(sck_id) >= 0)
    })
}

// HGET - returns the value for a specified key field in a hash
function getRoomMetadataValue(room, key, cb) {
    cache.hget('deeptalk#/#' + room + '#metadata', key, function(err, val) {
        log_cache.debug({
            method: 'getRoomMetadataValue',
            args: {room: room, key: key},
            err: err, val: val
        })
        if (err) {
            log_cache.error({err: err})
            cb(null)
        } else {
            cb(val)
        }
    })
}

// HMSET - sets values for specified key fields in a hash
// kvs - an array of alternating key, value pairs
// example kvs - [key1, val1, key2, val2]
function setRoomMetadataValues(room, kvs, cb) {
    cache.hmset('deeptalk#/#' + room + '#metadata', kvs, function(err, code) {
        log_cache.debug({
            method: 'setRoomMetadataValues',
            args: {room: room, kvs: kvs},
            err: err, code: code
        })
        if (err) {
            log_cache.error({err: err})
            cb(null)
        } else {
            cb(code)
        }
    })
}

// Secures a room
function secureRoom(room, cb) {
    getRoomMetadataValue(room, 'secure', function(m_secure) {
        if (m_secure == 'true') {
            cb(null)
        } else {
            password = uuid.v4();
            setRoomMetadataValues(room,
                                  ['secure', true,
                                   'password', password],
                                  function(code) {
                cb(password)
            })
        }
    })
}

// Unsecures a room
function unsecureRoom(room, password, override, cb) {
    if (!room)
        return cb(true)

    getRoomMetadataValue(room, 'secure', function(m_secure) {
        if (m_secure == 'false') {
            cb(true)
        } else {
            getRoomMetadataValue(room, 'password', function(m_password) {
                if (password === m_password || override) {
                    setRoomMetadataValues(room,
                                          ['secure', false],
                                          function(code) {
                        cb(true)
                    })
                } else {
                    cb(false)
                }
            })
        }
    })
}

function errorAccessCodeRequired(sck) {
    sck.emit('room-needs-access-code')
}

function errorBadAccessCode(sck) {
    sck.emit('room-invalid-access-code')
}

io.on('connection', function(sck) {
	log.debug({socket: sck.id}, 'io.connection()');

	var room = null;

	sck.on('join-room', function(_room, password){
		log.debug({
            socket: sck.id,
            room: _room,
            password: password
        }, 'io.join-room()');

        room = _room
		sck.emit('msg-to-user', 'Welcome to [' + room + ']');

        // Check if the room is secured or not
        getRoomMetadataValue(room, 'secure', function(m_secure) {
            // Reveal room security
            m_secure = (m_secure == 'true')
            sck.emit('stats-security', m_secure);
            // Room is secured
            if (m_secure) {
                // Don't reveal the amount of users within a secured room
                sck.emit('stats-users', 1);
                if (!password) {
                    // No password provided - this is first contact
                    // Notify the would-be user they need to provide an access code to enter
                    return errorAccessCodeRequired(sck);
                } else {
                    getRoomMetadataValue(room, 'password', function(m_pasword) {
                        if (password !== m_password) {
                            // Notify the would-be user they provided an incorrect access code
                            return errorBadAccessCode(sck);
                        }
                    })
                }
            }

            // The user has been allowed access to the room.  Let's welcome them
            sck.emit('room-join-success')
            // Actually join the user to the room
            addMemberToRoom(sck, room, function(member_count) {
                // Reveal / update the real number of users in the room
                sck.emit('stats-users', member_count);
                sck.to(room).emit('stats-users', member_count);
            })
            // Notify other users of the new user
            sck.to(room).emit('msg-to-room', sck.id, 'has joined the room');
        })
	});

	// The user is sending a message to their room
	sck.on('msg-to-room', function (_msg) {
	    log.debug({ socket: sck.id, room: room, message: _msg }, 'io.msg-to-room()');
        hasMembershipToRoom(sck.id, room, function(is_member) {
            if (!is_member) {
                return errorAccessCodeRequired(sck);
            }

            _msg = _msg.trim()
            if (_msg === '/secure') {
                secureRoom(room, function(password) {
                    if ( !password ) {
                        sck.emit('msg-repeat-to-self', 'Room is already secured');
                    } else {
                        sck.emit('stats-security', true);
                        sck.to(room).emit('stats-security', true);
                        sck.emit('msg-repeat-to-self',
                                 'Room is secured. Password: "' + password + '"');
                    }
                })
            } else if (_msg.lastIndexOf('/unsecure', 0) === 0) {
                parts = _msg.trim().split(' ');
                passwd = undefined
                if (parts.length == 2)
                    passwd = parts[1];
                else if (parts.length > 2) {
                    sck.emit('msg-repeat-to-self',
                             'Usage: /unsecure [password]');
                }

                getRoomMembership(room, function(members) {
                    unsecureRoom(room, passwd, (members.length === 1), function(is_unsecure) {
                        if (is_unsecure) {
                            sck.emit('stats-security', false);
                            sck.to(room).emit('stats-security', false);
                            sck.emit('msg-repeat-to-self', 'Room is unsecured');
                        } else {
                            sck.emit('msg-repeat-to-self',
                                     'Room could not be unsecured. Invalid password.');
                        }
                    })
                });
            } else {
                sck.emit('msg-repeat-to-self', _msg);
                sck.to(room).emit('msg-to-room', sck.id, _msg);
            }
        })
	});

	// The user has disconnected
    sck.on('disconnect', function(){
		log.debug({socket: sck.id}, "io.disconnect()");

		sck.to(room).emit('msg-to-room', sck.id, 'has left the room');
        removeMemberFomRoom(sck, room, function() {
            getRoomMembership(room, function(members) {
                sck.to(room).emit('stats-users', members.length);
                if (!members.length)
                    unsecureRoom(room, null, true, function(status){});
            });
        });
	});
});

// Start the service
server.listen(SVC_PORT, function() {
    log.debug({hostInfo: server.address()}, 'server.listen()');
});
